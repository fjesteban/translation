import React from 'react';
import logo from './logo.svg';
import './App.css';
import './i18n';
import i18next from "i18next";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          {i18next.t('titulo')}
        </a>
      </header>
      {/*<p>{i18next.t('texto')}</p>*/}
      {/*  <p>{i18next.t('parrafo')}</p>*/}
      <p>{i18next.t('login.texto.uno')}</p>
      <p>{i18next.t('login.parrafo.uno')}</p>
      <p>{i18next.t('login.texto.dos')}</p>
      <p>{i18next.t('login.parrafo.dos')}</p>
      <p>{i18next.t('test.text.first')}</p>
    </div>
  );
}

export default App;
