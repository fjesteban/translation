const fs = require('fs');
const chalk = require('chalk');

module.exports = {
  input: [
    'src/**/*.{js,jsx,tsx,ts}',
    // Use ! to filter out files or directories
    '!app/**/*.spec.{js,jsx}',
    '!app/i18n/**',
    '!**/node_modules/**',
  ],
  output: './',
  trans: {
    component: 'Trans',
    i18nKey: 'i18nKey',
    defaultsKey: 'defaults',
    extensions: ['.js', '.jsx', '.tsx', '.ts'],
    fallbackKey: function(ns, value) {
      return value;
    },
    acorn: {
      ecmaVersion: 11, // defaults to 10
      sourceType: 'module', // defaults to 'module'
      // Check out https://github.com/acornjs/acorn/tree/master/acorn#interface for additional options
    },
  },
  options: {
    debug: true,
    func: {
      list: ['i18next.t', 'i18n.t', 'react-i18next.t'],
      extensions: ['.js', '.jsx', '.tsx'],
    },
    lngs: ['en', 'es'],
    ns: ['translation'],
    defaultLng: 'es',
    defaultNs: 'translation',
    defaultValue: '',
    resource: {
      loadPath: 'src/locales/{{lng}}/{{ns}}.json',
      savePath: 'src/locales/{{lng}}/{{ns}}.json',
      jsonIndent: 2,
      lineEnding: '\n',
    },
    nsSeparator: ':', // namespace separator
    keySeparator: '.', // key separator
    interpolation: {
      prefix: '{{',
      suffix: '}}',
    },
  }
};
